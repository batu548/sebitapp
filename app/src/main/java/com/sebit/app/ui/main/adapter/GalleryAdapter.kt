package com.sebit.app.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.sebit.app.R
import com.sebit.app.data.model.ContentImage
import com.sebit.app.databinding.ItemGalleryBinding

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
class GalleryAdapter(private val items: MutableList<ContentImage>? = mutableListOf()) :
    RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ItemGalleryBinding>(
                inflater,
                R.layout.item_gallery,
                parent,
                false
            )
        return GalleryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        holder.binding.apply {
            contentImage = items!![position]
            executePendingBindings()
        }
    }

    override fun getItemCount() = items?.size ?: 0

    class GalleryViewHolder(val binding: ItemGalleryBinding) :
        RecyclerView.ViewHolder(binding.root)
}