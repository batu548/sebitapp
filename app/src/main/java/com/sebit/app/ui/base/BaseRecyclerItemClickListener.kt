package com.sebit.app.ui.base

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
interface BaseRecyclerItemClickListener<T> {
    fun onItemClick(item: T)
}