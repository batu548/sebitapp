package com.sebit.app.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.sebit.app.R
import com.sebit.app.data.model.Content
import com.sebit.app.databinding.ItemContentBinding
import com.sebit.app.ui.base.BaseRecyclerItemClickListener

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
class ContentAdapter(private val items: MutableList<Content> = mutableListOf()) :
    RecyclerView.Adapter<ContentAdapter.ContentViewHolder>() {

    private var itemClickListener: BaseRecyclerItemClickListener<Content>? = null

    fun setOnItemClickListener(itemClickListener: BaseRecyclerItemClickListener<Content>) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ItemContentBinding>(
                inflater,
                R.layout.item_content,
                parent,
                false
            )
        return ContentViewHolder(binding).apply {
            binding.root.setOnClickListener {
                val position = adapterPosition.takeIf { it != RecyclerView.NO_POSITION }
                    ?: return@setOnClickListener
                itemClickListener?.onItemClick(items[position])
            }
        }
    }

    override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {
        holder.binding.apply {
            content = items[position]
            executePendingBindings()
        }
    }

    override fun getItemCount() = items.size

    class ContentViewHolder(val binding: ItemContentBinding) :
        RecyclerView.ViewHolder(binding.root)
}