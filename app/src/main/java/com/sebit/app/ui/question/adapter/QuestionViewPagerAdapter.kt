package com.sebit.app.ui.question.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.sebit.app.R
import com.sebit.app.data.model.ContentQuestion
import com.sebit.app.databinding.ViewQuestionBinding

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
class QuestionViewPagerAdapter(
    private val layoutInflater: LayoutInflater,
    private val questions: List<ContentQuestion>?
) : PagerAdapter() {
    override fun getCount(): Int {
        return questions?.size ?: 0
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding = DataBindingUtil.inflate<ViewQuestionBinding>(
            layoutInflater,
            R.layout.view_question,
            container,
            false
        )

        val question = questions?.get(position)
        question?.html?.let {
            binding.webView.apply {
                loadDataWithBaseURL(
                    null,
                    it, "text/html", "UTF-8", null
                )
                settings.loadWithOverviewMode = true
                settings.useWideViewPort = true
                settings.displayZoomControls = false
                settings.builtInZoomControls = true
                settings.setSupportZoom(true)
                setBackgroundColor(Color.TRANSPARENT)
                settings.domStorageEnabled = true
                settings.javaScriptEnabled = true
                settings.javaScriptCanOpenWindowsAutomatically = true
                addJavascriptInterface(JavaScriptInterface(context), "SebitInterface")
            }
        }

        container.addView(binding.root)
        return binding.root
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }

    private inner class JavaScriptInterface(val context: Context) {
        @JavascriptInterface
        fun showPreloader() {
            Toast.makeText(context, "showPreloader", Toast.LENGTH_LONG).show()
        }

        @JavascriptInterface
        fun hidePreloader() {
            Toast.makeText(context, "hidePreloader", Toast.LENGTH_LONG).show()
        }

        @JavascriptInterface
        fun selectChoice(choice: String) {
            Toast.makeText(context, "selectChoice = $choice", Toast.LENGTH_LONG).show()
        }
    }
}