package com.sebit.app.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sebit.app.data.model.Content
import com.sebit.app.repository.ContentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@HiltViewModel
class MainViewModel @Inject constructor(
    private val contentRepository: ContentRepository
) : ViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    private val _error = MutableLiveData<Throwable>()
    private val _contents = MutableLiveData<MutableList<Content>>()

    val loading: LiveData<Boolean> = _loading
    val error: LiveData<Throwable> = _error
    val contents: LiveData<MutableList<Content>> = _contents

    fun getContents() {
        _loading.value = true
        viewModelScope.launch {
            contentRepository.getContents(
                onSuccess = {
                    _loading.value = false
                    _contents.value = it
                },
                onError = {
                    _loading.value = false
                    _error.value = it
                }
            )
        }
    }

}