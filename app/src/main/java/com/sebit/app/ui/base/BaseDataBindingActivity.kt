package com.sebit.app.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import io.reactivex.disposables.Disposables

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
abstract class BaseDataBindingActivity<ViewBinding : ViewDataBinding> : AppCompatActivity() {

    protected lateinit var binding: ViewBinding

    protected var disposable = Disposables.disposed()

    @LayoutRes
    abstract fun getLayoutRes(): Int
    abstract fun init()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutRes())
        binding.lifecycleOwner = this
        init()
    }

    override fun onDestroy() {
        binding.unbind()
        disposable.dispose()
        super.onDestroy()
    }
}