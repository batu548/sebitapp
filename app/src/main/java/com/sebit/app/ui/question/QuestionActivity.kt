package com.sebit.app.ui.question

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.sebit.app.R
import com.sebit.app.data.model.Content
import com.sebit.app.databinding.ActivityQuestionBinding
import com.sebit.app.ui.base.BaseDataBindingActivity
import com.sebit.app.ui.question.adapter.QuestionViewPagerAdapter
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@AndroidEntryPoint
class QuestionActivity : BaseDataBindingActivity<ActivityQuestionBinding>() {

    private lateinit var content: Content
    private lateinit var adapter: QuestionViewPagerAdapter

    override fun getLayoutRes(): Int {
        return R.layout.activity_question
    }

    override fun init() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        supportActionBar?.setDisplayShowHomeEnabled(true);

        content = intent.getSerializableExtra(EXTRA_CONTENT) as Content

        adapter = QuestionViewPagerAdapter(layoutInflater, content.contentQuestions)
        binding.vpQuestions.adapter = adapter

        binding.btnNext.setOnClickListener {
            if (content.contentQuestions?.size ?: 0 > binding.vpQuestions.currentItem + 1) {
                binding.vpQuestions.currentItem = binding.vpQuestions.currentItem + 1
            }
        }

        binding.btnPrevious.setOnClickListener {
            if (binding.vpQuestions.currentItem != 0) {
                binding.vpQuestions.currentItem = binding.vpQuestions.currentItem - 1
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    companion object {
        const val EXTRA_CONTENT = "EXTRA_CONTENT"

        fun newIntent(activity: AppCompatActivity, content: Content): Intent {
            val intent = Intent(activity, QuestionActivity::class.java)
            intent.putExtra(EXTRA_CONTENT, content)
            return intent
        }
    }
    
}