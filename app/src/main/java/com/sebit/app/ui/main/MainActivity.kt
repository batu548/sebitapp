package com.sebit.app.ui.main

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.FileProvider
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.sebit.app.BuildConfig
import com.sebit.app.R
import com.sebit.app.data.cache.Prefs
import com.sebit.app.data.downloader.PdfDownloader
import com.sebit.app.data.model.Content
import com.sebit.app.data.model.ContentType
import com.sebit.app.databinding.ActivityMainBinding
import com.sebit.app.extensions.fileName
import com.sebit.app.ui.base.BaseDataBindingActivity
import com.sebit.app.ui.base.BaseRecyclerItemClickListener
import com.sebit.app.ui.main.adapter.ContentAdapter
import com.sebit.app.ui.question.QuestionActivity
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.BackpressureStrategy
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseDataBindingActivity<ActivityMainBinding>(),
    BaseRecyclerItemClickListener<Content> {

    @Inject
    lateinit var prefs: Prefs

    private val mainViewModel: MainViewModel by viewModels()

    override fun getLayoutRes(): Int {
        return R.layout.activity_main
    }

    override fun init() {
        binding.vm = mainViewModel
        binding.executePendingBindings()

        mainViewModel.getContents()

        mainViewModel.contents.observe(this, {
            val adapter = ContentAdapter(it)
            adapter.setOnItemClickListener(this)
            binding.rvContent.adapter = adapter
        })
    }

    override fun onItemClick(item: Content) {
        when (item.contentType) {
            ContentType.PDF.type -> {
                downloadAndOpenPdf(item)
            }
            ContentType.VIDEO.type -> {
                // hard to develop whole project in 24 hours, enough for one day :)
            }
            ContentType.QUESTION.type -> {
                startActivity(QuestionActivity.newIntent(this, item))
            }
        }
    }

    private fun downloadAndOpenPdf(item: Content) {
        // check external storage permissions
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    if (p0?.deniedPermissionResponses.isNullOrEmpty()) {
                        item.fileUrl?.let { checkPdf(it) }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }

            }).withErrorListener {
                Toast.makeText(this, it.name, Toast.LENGTH_SHORT).show()
            }.check()
    }

    private fun checkPdf(pdfUrl: String) {
        // check pdf file exists and file download time
        val pdfFile = File(cacheDir, pdfUrl.fileName())
        if (pdfFile.exists() && prefs.getPdfDownloadTime(pdfUrl) + 600000 > System.currentTimeMillis()) {
            openPdf(pdfFile)
        } else {
            disposable = PdfDownloader().download(pdfUrl, pdfFile)
                .throttleFirst(2, TimeUnit.SECONDS)
                .toFlowable(BackpressureStrategy.LATEST)
                .subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe({
                    Toast.makeText(this, "Downloading progress %$it ", Toast.LENGTH_SHORT).show()
                }, {
                    Toast.makeText(this, it.localizedMessage, Toast.LENGTH_SHORT).show()
                }, {
                    Toast.makeText(this, "Download completed", Toast.LENGTH_SHORT).show()
                    prefs.setPdfDownloadTime(pdfUrl, System.currentTimeMillis())
                    openPdf(pdfFile)
                })
        }
    }

    private fun openPdf(pdfFile: File) {
        // we can use third party pdf viewers or write our own native pdf viewer
        // open with device's pdf viewer application for now :)

        val pdfPath =
            FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", pdfFile)
        val pdfIntent = Intent(Intent.ACTION_VIEW)
        pdfIntent.setDataAndType(pdfPath, "application/pdf")
        pdfIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

        try {
            startActivity(pdfIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                this,
                "Could not found any pdf viewer application",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

}