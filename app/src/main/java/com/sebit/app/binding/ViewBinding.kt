package com.sebit.app.binding

import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.sebit.app.extensions.visible

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@BindingAdapter("visible")
fun bindVisible(view: View, visible: Boolean) {
    view.visible(visible)
}

@BindingAdapter("throwable")
fun bindError(view: View, throwable: Throwable?) {
    val error = throwable?.message
    if (!TextUtils.isEmpty(error)) {
        Toast.makeText(view.context, error, Toast.LENGTH_SHORT).show()
    }
}

@BindingAdapter("imageUrl")
fun bindImageUrl(imageView: AppCompatImageView, url: String) {
    val requestOptions = RequestOptions()
        .override(imageView.width, imageView.height)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .transform(CenterCrop(), RoundedCorners(5))
    Glide.with(imageView.context)
        .load(url)
        .centerCrop()
        .apply(requestOptions)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(imageView)
}