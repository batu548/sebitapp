package com.sebit.app.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sebit.app.data.model.ContentImage
import com.sebit.app.ui.main.adapter.GalleryAdapter

/**
 * Created by Batuhan Coskun on 07 February 2021
 */

@BindingAdapter("contentImages")
fun bindContentImages(view: RecyclerView, contentImages: MutableList<ContentImage>?) {
    if (!contentImages.isNullOrEmpty()) {
        view.adapter = GalleryAdapter(contentImages)
    }
}