package com.sebit.app.di

import com.sebit.app.data.service.ContentService
import com.sebit.app.repository.ContentRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    @ViewModelScoped
    fun providesContentRepository(contentService: ContentService): ContentRepository {
        return ContentRepository(contentService)
    }

}