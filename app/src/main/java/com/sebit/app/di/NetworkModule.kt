package com.sebit.app.di

import com.sebit.app.data.api.ApiProvider
import com.sebit.app.data.service.ContentService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideApiProvider(): ApiProvider {
        return ApiProvider()
    }

    @Singleton
    @Provides
    fun provideContentService(apiProvider: ApiProvider) =
        apiProvider.create(ContentService::class.java)

}