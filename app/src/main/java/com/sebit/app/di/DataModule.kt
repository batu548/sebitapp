package com.sebit.app.di

import android.app.Application
import com.sebit.app.data.cache.Prefs
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun providePrefs(application: Application): Prefs {
        return Prefs(application)
    }

}