package com.sebit.app.repository

import androidx.annotation.WorkerThread
import com.sebit.app.data.model.Content
import com.sebit.app.data.service.ContentService
import javax.inject.Inject


/**
 * Created by Batuhan Coskun on 07 February 2021
 */
class ContentRepository @Inject constructor(
    private val contentService: ContentService
) {
    @WorkerThread
    suspend fun getContents(
        onSuccess: (MutableList<Content>) -> Unit,
        onError: (Throwable?) -> Unit
    ) {
        runCatching {
            contentService.getContents()
        }.onSuccess { response ->
            onSuccess(response)
        }.onFailure { throwable ->
            throwable.printStackTrace()
            onError(throwable)
        }
    }

}