package com.sebit.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@HiltAndroidApp
class App : Application()