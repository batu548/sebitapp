package com.sebit.app.extensions

import android.view.View
import java.net.MalformedURLException
import java.net.URL

/**
 * Created by Batuhan Coskun on 07 February 2021
 */

fun View.visible(visible: Boolean) {
    visibility = if (visible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

fun String.fileName(): String {
    try {
        val resource = URL(this)
        val host = resource.host
        if (host.isNotEmpty() && this.endsWith(host)) {
            return ""
        }
    } catch (e: MalformedURLException) {
        return ""
    }

    val startIndex = this.lastIndexOf('/') + 1
    val length = this.length

    var lastQMPos = this.lastIndexOf('?')
    if (lastQMPos == -1) {
        lastQMPos = length
    }

    var lastHashPos = this.lastIndexOf('#')
    if (lastHashPos == -1) {
        lastHashPos = length
    }

    val endIndex = kotlin.math.min(lastQMPos, lastHashPos)
    return this.substring(startIndex, endIndex)
}