package com.sebit.app.utils

import android.util.Log
import com.squareup.moshi.Moshi

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
object MoshiUtils {

    val moshi: Moshi by lazy {
        Moshi.Builder().build()
    }

    fun toJson(source: Any): String {
        return moshi.adapter(source.javaClass).toJson(source)
    }


    inline fun <reified T : Any> fromJson(json: String): T? {
        return try {
            return moshi.adapter(T::class.java).fromJson(json)
        } catch (ex: Exception) {
            Log.e("Moshi", "Moshi parse error: $ex")
            null
        }
    }
    
}