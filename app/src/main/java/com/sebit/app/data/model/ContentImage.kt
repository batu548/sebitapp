package com.sebit.app.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@JsonClass(generateAdapter = true)
data class ContentImage(
    @Json(name = "thumb") val thumbUrl: String,
    @Json(name = "original") val originalUrl: String
) : Serializable