package com.sebit.app.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@JsonClass(generateAdapter = true)
data class Content(
    @Json(name = "contentType") val contentType: String,
    @Json(name = "title") val title: String,
    @Json(name = "desciription") val description: String?,
    @Json(name = "fileUrl") val fileUrl: String?,
    @Json(name = "videoUrl") val videoUrl: String?,
    @Json(name = "images") val contentImages: List<ContentImage>?,
    @Json(name = "questions") val contentQuestions: List<ContentQuestion>?,
    @Json(name = "tags") val contentVideoTags: List<ContentVideoTag>?,
) : Serializable {

    fun isPdf(): Boolean {
        return contentType == ContentType.PDF.type
    }

    fun isGallery(): Boolean {
        return contentType == ContentType.GALLERY.type
    }

    fun isQuestion(): Boolean {
        return contentType == ContentType.QUESTION.type
    }

    fun isVideo(): Boolean {
        return contentType == ContentType.VIDEO.type
    }

    fun getTypeDescription(): String {
        return "Type: $contentType"
    }

    fun hasDescription(): Boolean {
        return !description.isNullOrEmpty()
    }

}