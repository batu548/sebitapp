package com.sebit.app.data.cache

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.sebit.app.utils.MoshiUtils

/**
 * Created by Batuhan Coskun on 07 February 2021
 */

class Prefs(context: Context) {

    private val preferences = getSharedPreferences(context)

    companion object {

        fun getSharedPreferences(context: Context): SharedPreferences =
            context.getSharedPreferences("SebitApp", Context.MODE_PRIVATE)
    }

    fun clear() {
        preferences.edit().clear().apply()
    }

    fun getPdfDownloadTime(pdfUrl: String): Long {
        return preferences.get(pdfUrl) ?: 0L
    }

    fun setPdfDownloadTime(pdfUrl: String, time: Long) {
        preferences.set(pdfUrl, time)
    }
}

/**
 * Finds value on given key.
 *
 * [T] is the type of value
 *
 * @param key          The name of the preference.
 * @param defaultValue Optional default value - will take null for strings, false for bool and -1 for numeric values if [defaultValue] is not specified
 * @return The value associated with this key, defValue if there isn't any
 */
inline fun <reified T : Any> SharedPreferences.get(key: String, defaultValue: T? = null): T? {
    return when (T::class) {
        String::class -> getString(key, defaultValue as? String) as T?
        Int::class -> getInt(key, defaultValue as? Int ?: -1) as T?
        Boolean::class -> getBoolean(key, defaultValue as? Boolean ?: false) as T?
        Float::class -> getFloat(key, defaultValue as? Float ?: -1f) as T?
        Long::class -> getLong(key, defaultValue as? Long ?: -1) as T?
        Set::class -> getStringSet(key, setOf()) as T?
        else -> getString(key, null)?.let {
            MoshiUtils.fromJson<T>(it)
        }
    }
}

/**
 * Puts a key value pair in shared prefs if doesn't exists, otherwise updates value on given [key].
 *
 * @param key   The name of the preference.
 * @param value The new set for the preference.
 */
fun SharedPreferences.set(key: String, value: Any?, immediately: Boolean = false) {
    when (value) {
        is String? -> edit(immediately) { it.putString(key, value) }
        is Int -> edit(immediately) { it.putInt(key, value) }
        is Boolean -> edit(immediately) { it.putBoolean(key, value) }
        is Float -> edit(immediately) { it.putFloat(key, value) }
        is Long -> edit(immediately) { it.putLong(key, value) }
        else -> edit(immediately) {
            it.putString(
                key,
                value?.let { it1 -> MoshiUtils.toJson(it1) })
        }
    }
}

@SuppressLint("ApplySharedPref")
private fun SharedPreferences.edit(
    immediately: Boolean = false,
    operation: (SharedPreferences.Editor) -> Unit
) {
    val editor = this.edit()
    operation(editor)

    when (immediately) {
        true -> editor.commit()
        else -> editor.apply()
    }
}