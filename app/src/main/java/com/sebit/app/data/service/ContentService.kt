package com.sebit.app.data.service

import com.sebit.app.data.model.Content
import retrofit2.http.GET

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
interface ContentService {

    @GET("/androiddata.json")
    suspend fun getContents(): MutableList<Content>

}