package com.sebit.app.data.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
class ApiProvider() {

    companion object {
        private const val CONNECT_TIME_OUT = 40L
        private const val READ_TIME_OUT = 40L
    }

    private val retrofit: Retrofit

    init {
        retrofit = Retrofit.Builder()
            .baseUrl("https://sebit.s3.eu-central-1.amazonaws.com/")
            .client(makeOkHttpClient())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    private fun makeOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(createHeaderInterceptor())
            .addInterceptor(createLoggingInterceptor())
            .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
            .build()
    }

    private fun createLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    private fun createHeaderInterceptor(): Interceptor {
        return Interceptor.invoke { chain ->
            val originalRequest = chain.request()

            val newRequestBuilder = originalRequest.newBuilder()

            for (entry in getHeaders().entries) {
                newRequestBuilder.addHeader(entry.key, entry.value)
            }

            chain.proceed(newRequestBuilder.build())
        }
    }

    private fun getHeaders(): Map<String, String> {
        return HashMap<String, String>().apply {
            put("Accept", "application/json")
            put("Content-Type", "application/json")
        }
    }

    fun <S> create(serviceClass: Class<S>): S {
        return retrofit.create(serviceClass)
    }
}