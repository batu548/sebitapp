package com.sebit.app.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
@JsonClass(generateAdapter = true)
data class ContentVideoTag(
    @Json(name = "html") val html: String,
    @Json(name = "positionToGo") val positionToGo: Int
) : Serializable