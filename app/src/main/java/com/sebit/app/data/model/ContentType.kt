package com.sebit.app.data.model

/**
 * Created by Batuhan Coskun on 07 February 2021
 */
enum class ContentType(val type: String) {
    PDF("pdf"),
    GALLERY("gallery"),
    QUESTION("question"),
    VIDEO("video")
}